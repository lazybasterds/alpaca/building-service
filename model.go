package main

import (
	"github.com/mongodb/mongo-go-driver/bson/primitive"
)

// Building contains information about the building
// where the system is deployed
type Building struct {
	ID          primitive.ObjectID   `bson:"_id,omitempty" json:"id,omitempty"`
	Name        string               `bson:"name" json:"name"`
	Location    string               `bson:"location" json:"location"`
	Description string               `bson:"description" json:"description"`
	ImageList   []primitive.ObjectID `bson:"image" json:"image"`
}

func toString(source *[]primitive.ObjectID) []string {
	result := make([]string, 0, len(*source))
	for _, s := range *source {
		result = append(result, s.Hex())
	}
	return result
}

// Floor contains information for a particular floor in a map
type Floor struct {
	ID   primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	Name string             `bson:"name" json:"name"`
}
